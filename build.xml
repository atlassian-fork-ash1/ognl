<?xml version="1.0"?>

<project name="ognl" default="jar" basedir=".">

    <!-- **************************************************************** -->
    <!-- *********************** Environment Setup ********************** -->
    <!-- **************************************************************** -->
    <!-- General environment setup -->
    <property environment="env"/>
    <property file="./build.properties"/>
    <property file="${user.home}/.build.properties"/>

    <!-- **************************************************************** -->
    <!-- ********************** Project Properties ********************** -->
    <!-- **************************************************************** -->
    <property name="project.root"             location="."/>
    <property name="project.name"             value="${ant.project.name}"/>
    <property name="project.title"            value="${project.name}"/>
    <property name="project.version"          value="1.0.0"/>
    <property name="project.version.suffix"   value="-${project.version}"/>
    <property name="project.lib"              location="${project.root}/lib"/>
    <property name="project.meta"             location="${project.root}/meta"/>

    <!-- **************************************************************** -->
    <!-- *********************** Output Properties ********************** -->
    <!-- **************************************************************** -->
    <property name="output.root"              location="build"/>
    <property name="output.dist"              location="${output.root}/dist"/>
    <property name="output.src.dist"          location="${output.dist}/${project.name}-dist"/>
    <property name="output.src.dist.build"    location="${output.src.dist}/build"/>
    <property name="output.src.dist.dist"     location="${output.src.dist}/dist"/>
    <property name="output.src.dist.etc"      location="${output.src.dist}/etc"/>
    <property name="output.src.dist.lib"      location="${output.src.dist}/lib"/>
    <property name="output.src.dist.src"      location="${output.src.dist}/src"/>

    <!-- **************************************************************** -->
    <!-- *************************** TaskDefs *************************** -->
    <!-- **************************************************************** -->
    <taskdef resource="net/sf/antcontrib/antcontrib.properties">
        <classpath>
            <pathelement location="${ant.contrib.jar}"/>
        </classpath>
    </taskdef>

    <!-- **************************************************************** -->
    <!-- *************** General Initialization & Targets *************** -->
    <!-- **************************************************************** -->
    <!-- ========================      Targets      ======================== -->
    <target name="init">
        <tstamp>
            <format property="DSTAMP" pattern="yyyyMMdd"/>
            <format property="TSTAMP" pattern="hhmmss"/>
        </tstamp>
    </target>

    <target name="info" depends="init">
        <echo message="*              JAVA_HOME: ${env.JAVA_HOME}"/>
        <echo message="*               ANT_HOME: ${env.ANT_HOME}"/>
        <echo message="*   NUMBER_OF_PROCESSORS: ${env.NUMBER_OF_PROCESSORS}"/>
        <echo message="*                   TEMP: ${env.TEMP}"/>
        <echo message="*"/>
        <echo message="*               ant.file: ${ant.file}"/>
        <echo message="*               ant.home: ${ant.home}"/>
        <echo message="*            ant.version: ${ant.version}"/>
        <echo message="*       ant.project.name: ${ant.project.name}"/>
        <echo message="*       ant.java.version: ${ant.java.version}"/>
        <echo message="*              user.home: ${user.home}"/>
        <echo message="*"/>
        <echo message="*           project.name: ${project.name}"/>
        <echo message="*          project.title: ${project.title}"/>
        <echo message="*        project.version: ${project.version}"/>
        <echo message="* project.version.suffix: ${project.version.suffix}"/>
        <echo message="*           project.root: ${project.root}"/>
        <echo message="*            project.lib: ${project.lib}"/>
        <echo message="*"/>
        <echo message="*            output.root: ${output.root}"/>
        <echo message="*            output.dist: ${output.dist}"/>
    </target>

    <!-- **************************************************************** -->
    <!-- **************************** Javac ***************************** -->
    <!-- **************************************************************** -->
    <property name="project.java"             location="${project.root}/java"/>
    <property name="output.classes"           location="${output.root}/classes"/>

    <property name="javac.compiler"           value="modern"/>
    <property name="javac.optimize"           value="off"/>
    <property name="javac.deprecation"        value="on"/>
    <property name="javac.debug"              value="on"/>
    <property name="build.compiler"           value="${javac.compiler}"/>

    <!-- ========================       Paths       ======================== -->
    <!-- Path including all Java libraries in the project's lib directory -->
    <path id="project.lib.path">
      <fileset dir="${project.root}">
        <include name="lib/**/*.jar"/>
      </fileset>
    </path>

    <path id="build.path">
      <pathelement location="${env.CLASSPATH}"/>
      <path refid="project.lib.path"/>
    </path>

    <path id="javac.default.class.path">
      <path refid="build.path"/>
      <pathelement location="${output.classes}"/>
    </path>

    <path id="javac.default.java.path">
      <pathelement location="${project.java}"/>
    </path>

    <!-- ========================    Patternsets    ======================== -->
    <patternset id="javac.non.java.files.pattern">
      <exclude name="**/*.java"/>
      <exclude name="**/*.java~"/>
      <exclude name="**/*.jav@"/>
      <exclude name="**/*.jjt"/>
      <exclude name="**/*.jj"/>
    </patternset>

    <!-- ======================== Internal Targets  ======================== -->
    <!--
        Copies all non-Java and non-JavaCC source files to the destination.
        This will keep all class-specific resources close to their users (like
        bundled resources).

        Parameters:

            javac.classes           Destination of the copy
                                    Defaults to ${output.classes}.

            javac.java              Source root from which resources will be copied.
                                    Defaults to ${project.java}.

            javac.files.pattern     Patternset reference files that will be included
                                    or excluded from the copy.
                                    Defaults to ${javac.non.java.files.pattern}.
    -->
    <patternset id="javac.files.pattern">
        <patternset refid="javac.non.java.files.pattern"/>
    </patternset>
    <target name="_javac-copy-resources">
        <property name="javac.java" value="${project.java}"/>
        <property name="javac.classes" value="${output.classes}"/>
        <copy todir="${javac.classes}">
            <fileset dir="${javac.java}">
                <patternset refid="javac.files.pattern"/>
            </fileset>
        </copy>
    </target>

    <!-- ========================      Targets      ======================== -->
    <target name="javac-info">
      <echo message="*           project.java: ${project.java}"/>
      <echo message="*         output.classes: ${output.classes}"/>
    </target>


    <!--
        Compiles all Java files in the given source directory to the destination
        directory then copies all non-Java files that may be resources of the
        classes to the destination directory.

        The destination directory will be created if necessary.

        Parameters:

            javac.classes           Destination root directory of compiled classes.
                                    Defaults to ${output.classes}.

            javac.java.path         Path to java sources to compile.
                                    Defaults to ${javac.default.java.path}.

            javac.class.path        Classpath to use to compile java classes.
                                    Defaults to ${javac.default.class.path}.

            javac.files.pattern     Patternset for copying class resources to destination.
                                    Defaults to ${javac.non.java.files.pattern}.
    -->
    <path id="javac.java.path">
        <path refid="javac.default.java.path"/>
    </path>
    <path id="javac.class.path">
        <path refid="javac.default.class.path"/>
    </path>
    <property name="javac.classes" value="${output.classes}"/>

    <target name="javac-compile">
        <mkdir dir="${javac.classes}"/>
        <javac destdir="${javac.classes}" optimize="${javac.optimize}" debug="${javac.debug}" deprecation="${javac.deprecation}" source="1.5">
            <src>
                <path refid="javac.java.path"/>
            </src>
            <classpath refid="javac.class.path"/>
        </javac>
        <pathconvert property="javac.java.path.list" refid="javac.java.path" pathsep=","/>
        <foreach target="_javac-copy-resources" list="${javac.java.path.list}" param="javac.java">
            <param name="javac.classes" value="${javac.classes}"/>
        </foreach>
    </target>

    <!-- **************************************************************** -->
    <!-- ************************ Jar Generation ************************ -->
    <!-- **************************************************************** -->

    <property name="project.jar"              value="${project.name}${project.version.suffix}.jar"/>
    <property name="project.dist.jar"         value="${project.name}${project.version.suffix}-dist.zip"/>

    <property name="output.jar"               location="${output.jars}/${project.jar}"/>
    <property name="output.dist.jar"          location="${output.jars}/${project.dist.jar}"/>

    <!-- ========================    Patternsets    ======================== -->
    <patternset id="jar.default.files.pattern"/>

    <!-- ======================== External Targets  ======================== -->
    <target name="jar-info">
      <echo message="*            output.jars: ${output.jars}"/>
      <echo message="*             output.jar: ${output.jar}"/>
    </target>

    <!--
        Creates a jar file given the parameters for base directory, jar file
        output and excluded and included files (via a patternset).

        Parameters:

            jar.jar             Jar file to be created.
                                Defaults to ${output.jar}.

            jar.basedir         Base directory from which files will be archived.
                                Defaults to ${output.classes}.

            jar.files.pattern   Patternset of the files to include or exclude.
                                Defaults to ${jar.default.files.pattern}.
    -->
    <patternset id="jar.files.pattern">
        <patternset refid="jar.default.files.pattern"/>
    </patternset>
    <target name="jar-build" depends="init">
        <property name="jar.basedir" value="${output.classes}"/>
        <property name="jar.jar" value="${output.jar}"/>
        <dirname property="jar.dir" file="${jar.jar}"/>
        <mkdir dir="${jar.dir}"/>
        <jar jarfile="${jar.jar}" basedir="${jar.basedir}">
            <patternset refid="jar.files.pattern"/>
        </jar>
    </target>

    <target name="jar-dist" depends="init">
        <mkdir dir="${output.jars}"/>
        <zip zipfile="${output.dist.jar}" basedir="${project.root}">
            <exclude name="**/*.zip"/>
            <exclude name="**/*.jar"/>
            <exclude name="build/**/*"/>
        </zip>
    </target>

    <!-- **************************************************************** -->
    <!-- **************************** JavaCC **************************** -->
    <!-- **************************************************************** -->

    <!-- ========================    Properties     ======================== -->
    <property name="project.javacc"             location="${project.root}/parser-generated"/>
    <property name="output.javacc.gen"          location="${output.root}/javacc/generated"/>
    <property file="javacc-${javacc.version}.properties"/>

    <!-- ========================       Paths       ======================== -->
    <path id="javacc.path">
        <pathelement location="${javacc.lib}"/>
    </path>

    <!-- ========================      Targets      ======================== -->
    <target name="_javacc-copy-generated" if="javacc.copy.generated.required">
    	<copy todir="${output.javacc.gen}">
    		<fileset dir="${javacc.gen}">
        		<exclude name="**/*.jj"/>
        		<exclude name="**/*.jjt"/>
            </fileset>
    	</copy>
    </target>

    <target name="_javacc-tree" if="javacc.tree.build.required">
    	<java classname="${javacc.tree}" classpathref="javacc.path" fork="true">
    		<arg value="-OUTPUT_DIRECTORY=${output.javacc.gen}/${javacc.pkg}"/>
    		<arg value="${javacc.jjt}"/>
    	</java>
    </target>

    <target name="_javacc-parser" if="javacc.parser.build.required">
    	<java classname="${javacc.parser}" classpathref="javacc.path" fork="true">
    		<arg value="-OUTPUT_DIRECTORY=${output.javacc.gen}/${javacc.pkg}"/>
    		<arg value="${javacc.jj}"/>
    	</java>
    </target>

    <!--
        ************************************************************************
        javacc-build

        Parameters:
            javacc.jj           JavaCC parser file to process
            javacc.jjt          JavaCC tree file to process (if javacc.jj is
                                not specified)
            javacc.gen          Directory of JavaCC generated files that have
                                been modified and subsequently need to be
                                included in the build.
            javacc.pkg          Name of package expressed as a path
        ************************************************************************
    -->
    <path id="javacc.gen.path">
        <pathelement location="${output.javacc.gen}"/>
    </path>

    <path id="javacc.gen.build.path">
        <path refid="javac.default.java.path"/>
        <path refid="javacc.gen.path"/>
    </path>
    <target name="javacc-build">
        <mkdir dir="${output.javacc.gen}"/>

        <!--
            Set up the javacc.gen.jj property - indicates we are generating
            jj file from jjt.
        -->
        <basename property="javacc.name" file="${javacc.jjt}" suffix=".jjt"/>
        <condition property="javacc.gen.jj" value="${output.javacc.gen}/${javacc.pkg}/${javacc.name}.jj">
            <isset property="javacc.jjt"/>
        </condition>

        <condition property="javacc.copy.generated.required">
            <and>
                <isset property="javacc.gen"/>
                <not>
            		<uptodate>
            			<srcfiles dir="${javacc.gen}" includes="**/*.java"/>
            			<mapper type="glob" from="*.java" to="${output.javacc.gen}/*.java"/>
            		</uptodate>
                </not>
            </and>
        </condition>
        <antcall target="_javacc-copy-generated"/>

        <!--
            Only need to call jjtree to generate jj file if the javacc.jjt
            property is set not up-to-date with the generated jj file.
        -->
        <condition property="javacc.tree.build.required">
            <and>
                <isset property="javacc.jjt"/>
                <not>
                    <uptodate srcfile="${javacc.jjt}" targetfile="${javacc.gen.jj}"/>
                </not>
            </and>
        </condition>

        <!--
            Need to call the parser if the project.javacc.jjt property is set (indicating that
            jjtree is run to produce a .jj file) AND if that file is now out of date wrt
            the .jjt file specified by project.javacc.jjt.
        -->
        <condition property="javacc.parser.build.required">
            <or>
                <and>
                    <isset property="javacc.jj"/>
                    <uptodate>
                        <srcfiles dir="${output.javacc.gen}"/>
            			<mapper type="merge" to="${javacc.jj}"/>
                    </uptodate>
                </and>
                <and>
                    <isset property="javacc.jjt"/>
                    <not>
                        <uptodate srcfile="${javacc.jjt}" targetfile="${javacc.gen.jj}"/>
                    </not>
                </and>
            </or>
        </condition>

        <antcall target="_javacc-tree"/>

        <!--
            Set up the javacc.jj property to the generated jj if not passed in already.
        -->
        <condition property="javacc.jj" value="${javacc.gen.jj}">
            <isset property="javacc.gen.jj"/>
        </condition>

        <antcall target="_javacc-parser"/>

    </target>

    <!-- **************************************************************** -->
    <!-- *************************** Javadoc **************************** -->
    <!-- **************************************************************** -->

    <!-- ========================    Properties     ======================== -->
    <property name="project.javadoc"        location="${project.root}/javadoc"/>
    <property name="output.javadoc"         location="${output.root}/javadoc"/>

    <!-- ========================       Paths       ======================== -->
    <path id="javadoc.default.java.path">
        <pathelement location="${project.java}"/>
    </path>

    <path id="javadoc.default.class.path">
        <path refid="javac.default.class.path"/>
    </path>

    <!-- ========================      Targets      ======================== -->
    <!--
        Builds javadoc documentation from the source paths given

        The destination directory will be created if necessary.

        Parameters:

            javadoc.java.path       Root of the source for which to generate docs.

            javadoc.pkglist         Comma-separated list of package names for which
                                    to generate docs.
                                    Default is ${project.javadoc.pkglist}.

            javadoc.class.path      Reference to classpath used to find referenced
                                    classes.
                                    Default to ${javadoc.default.class.path}.

            javadoc.output          Output directory of the generated documentation.
                                    Default is ${output.javadoc}.

            javadoc.title           Title of the generated documentation.
                                    Default is ${project.title}.

            javadoc.owner           Owner of the copyright on the documentation
                                    Default is ${project.owner}.

            javadoc.owner.url       Web link to the owner of the copyright on the
                                    documentation.
                                    Default is ${project.owner.url}.
    -->
    <path id="javadoc.java.path">
        <path refid="javadoc.default.java.path"/>
    </path>
    <path id="javadoc.class.path">
        <path refid="javadoc.default.class.path"/>
    </path>
    <target name="javadoc-doc">
      <condition property="javadoc.pkglist" value="${project.javadoc.pkglist}">
          <isset property="project.javadoc.pkglist"/>
      </condition>
      <condition property="javadoc.pkglist" value="*">
          <not>
              <isset property="project.javadoc.pkglist"/>
          </not>
      </condition>
      <property name="javadoc.title" value="${project.title}"/>
      <property name="javadoc.owner.url" value="${project.owner.url}"/>
      <property name="javadoc.owner" value="${project.owner}"/>
      <property name="javadoc.output" value="${output.javadoc}"/>
      <property name="javadoc.overview" value="${project.root}/javadoc/package.html"/>

    	<mkdir dir="${javadoc.output}"/>
      <javadoc packagenames="${javadoc.pkglist}" destdir="${javadoc.output}" version="false" overview="${javadoc.overview}" windowtitle="${javadoc.title}">
        <sourcepath refid="javadoc.java.path"/>
        <classpath refid="javadoc.class.path"/>
        <doctitle><![CDATA[<h1>${javadoc.title}</h1>]]></doctitle>
        <bottom><![CDATA[<i>Copyright &#169; 2004 <a href="${project.owner.url}">${project.owner}</a>. All Rights Reserved.</i>]]></bottom>
      </javadoc>
    </target>

    <!-- **************************************************************** -->
    <!-- *************************** Docbook **************************** -->
    <!-- **************************************************************** -->

    <!-- ==================== Initialization properties ===================== -->
    <!-- Project information -->
    <property name="project.docbook"            location="${project.root}/docbook"/>
    <property name="project.docbook.filelist"   value="LanguageGuide,DeveloperGuide"/>

    <!-- Build results setup -->
    <property name="output.docbook.style"       location="${output.root}/docbook/style"/>
    <property name="output.docbook.fop"         location="${output.root}/docbook/fop"/>
    <property name="output.docbook.html"        location="${output.root}/docbook/html"/>
    <property name="output.docbook.pdf"         location="${output.root}/docbook/pdf"/>

    <!-- DocBook -->

    <property name="docbook.xml.dir"            location="${docbook.xml.path}"/>
    <property name="docbook.xsl.dir"            location="${docbook.xsl.path}"/>
    <property name="docbook.html.chunked.xsl"   location="${output.docbook.style}/${docbook.xsl.base}-html-chunked.xsl"/>
    <property name="docbook.fop.xsl"            location="${output.docbook.style}/${docbook.xsl.base}-fop.xsl"/>
    <property name="fop.dir"                    location="${fop.path}"/>

    <xmlcatalog id="docbook.catalog">
        <dtd publicId="-//OASIS//DTD DocBook XML V${docbook.xml.version}//EN" location="${docbook.xml.dir}/docbookx.dtd"/>
        <dtd publicId="-//OASIS//DTD DocBook CALS Table Model V${docbook.xml.version}//EN" location="${docbook.xml.dir}/calstblx.dtd"/>
        <dtd publicId="-//OASIS//DTD XML Exchange Table Model 19990315//EN" location="${docbook.xml.dir}/soextblx.dtd"/>
        <dtd publicId="-//OASIS//ELEMENTS DocBook Information Pool V${docbook.xml.version}//EN" location="${docbook.xml.dir}/dbpoolx.mod"/>
        <dtd publicId="-//OASIS//ELEMENTS DocBook Document Hierarchy V${docbook.xml.version}//EN" location="${docbook.xml.dir}/dbhierx.mod"/>
        <dtd publicId="-//OASIS//ENTITIES DocBook Additional General Entities V${docbook.xml.version}//EN" location="${docbook.xml.dir}/dbgenent.mod"/>
        <dtd publicId="-//OASIS//ENTITIES DocBook Notations V${docbook.xml.version}//EN" location="${docbook.xml.dir}/dbnotnx.mod"/>
        <dtd publicId="-//OASIS//ENTITIES DocBook Character Entities V${docbook.xml.version}//EN" location="${docbook.xml.dir}/dbcentx.mod"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Diacritical Marks//EN//XML" location="${docbook.xml.dir}/ent/iso-dia.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN//XML" location="${docbook.xml.dir}/ent/iso-num.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Publishing//EN//XML" location="${docbook.xml.dir}/ent/iso-pub.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES General Technical//EN//XML" location="${docbook.xml.dir}/ent/iso-tech.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Latin 1//EN//XML" location="${docbook.xml.dir}/ent/iso-lat1.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Latin 2//EN//XML" location="${docbook.xml.dir}/ent/iso-lat2.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Greek Letters//EN//XML" location="${docbook.xml.dir}/ent/iso-grk1.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Monotoniko Greek//EN//XML" location="${docbook.xml.dir}/ent/iso-grk2.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Greek Symbols//EN//XML" location="${docbook.xml.dir}/ent/iso-grk3.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN//XML" location="${docbook.xml.dir}/ent/iso-grk4.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Arrow Relations//EN//XML" location="${docbook.xml.dir}/ent/iso-amsa.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Binary Operators//EN//XML" location="${docbook.xml.dir}/ent/iso-amsb.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Delimiters//EN//XML" location="${docbook.xml.dir}/ent/iso-amsc.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Negated Relations//EN//XML" location="${docbook.xml.dir}/ent/iso-amsn.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Ordinary//EN//XML" location="${docbook.xml.dir}/ent/iso-amso.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Added Math Symbols: Relations//EN//XML" location="${docbook.xml.dir}/ent/iso-amsr.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Box and Line Drawing//EN//XML" location="${docbook.xml.dir}/ent/iso-box.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Russian Cyrillic//EN//XML" location="${docbook.xml.dir}/ent/iso-cyr1.ent"/>
        <dtd publicId="ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN//XML" location="${docbook.xml.dir}/ent/iso-cyr2.ent"/>
        <entity publicId="html/chunk.xsl" location="${docbook.xsl.dir}/html/chunk.xsl"/>
        <entity publicId="fo/docbook.xsl" location="${docbook.xsl.dir}/fo/docbook.xsl"/>
    </xmlcatalog>

    <!-- ============================= Targets ============================== -->
    <target name="docbook-init">
        <available property="docbook.xml.available" file="${docbook.xml.dir}" type="dir"/>
        <available property="docbook.xsl.available" file="${docbook.xsl.dir}" type="dir"/>
        <mkdir dir="${output.docbook.style}"/>
    </target>

    <target name="docbook-info">
        <echo message="*          project.docbook: ${project.docbook}"/>
        <echo message="*"/>
        <echo message="*         docbook.xml.base: ${docbook.xml.base}"/>
        <echo message="*          docbook.xml.dir: ${docbook.xml.dir}"/>
        <echo message="*         docbook.xsl.base: ${docbook.xsl.base}"/>
        <echo message="*          docbook.xsl.dir: ${docbook.xsl.dir}"/>
        <echo message="* docbook.html.chunked.xsl: ${docbook.html.chunked.xsl}"/>
        <echo message="*          docbook.fop.xsl: ${docbook.fop.xsl}"/>
        <echo message="*"/>
        <echo message="*                 fop.base: ${fop.base}"/>
        <echo message="*                  fop.dir: ${fop.dir}"/>
        <echo message="*"/>
        <echo message="*     output.docbook.style: ${output.docbook.style}"/>
        <echo message="*       output.docbook.fop: ${output.docbook.fop}"/>
        <echo message="*      output.docbook.html: ${output.docbook.html}"/>
        <echo message="*       output.docbook.pdf: ${output.docbook.pdf}"/>
    </target>

    <target name="docbook-clean">
        <delete file="${output.docbook.style}"/>
        <delete file="${output.docbook.html}"/>
        <delete file="${output.docbook.fop}"/>
        <delete file="${output.docbook.pdf}"/>
    </target>

    <!--
        Takes "docbook.file" as a parameter to the file to generate
     -->
    <target name="docbook-html-doc-single">
        <copy file="${project.docbook}/style/html-xsl.template" tofile="${docbook.html.chunked.xsl}">
            <filterset>
                <filter token="docbook.xsl.base" value="${docbook.xsl.base}"/>
                <filter token="docbook.xsl.dir" value="${docbook.xsl.dir}"/>
                <filter token="format" value="chunk"/>
            </filterset>
        </copy>
        <replace token="\" value="/" file="${docbook.html.chunked.xsl}"/>

        <copy todir="${output.docbook.html}">
            <fileset dir="${project.docbook}">
                <include name="**/*.gif"/>
                <include name="**/*.jpg"/>
                <include name="**/*.jpeg"/>
            </fileset>
        </copy>

        <copy todir="${output.docbook.html}" file="${project.docbook}/style/docbook.css"/>
        <uptodate property="uptodate.html" targetfile="${output.docbook.html}/${docbook.file}/index.html">
            <srcfiles dir="${project.docbook}">
                <include name="${docbook.file}.xml"/>
            </srcfiles>
        </uptodate>
        <antcall target="docbook-generate-html-chunked">
            <param name="docbook.file" value="${docbook.file}"/>
        </antcall>
    </target>

    <target name="docbook-generate-html-chunked" unless="uptodate.html">
        <style  basedir="${project.docbook}"
                style="${docbook.html.chunked.xsl}"
                processor="trax"
                force="true"
                in="${project.docbook}/${docbook.file}.xml"
                out="${output.docbook.html}/${docbook.file}/index.html">
            <param name="base.dir" expression="${output.docbook.html}/${docbook.file}/"/>
            <param name="chunk.quietly" expression="1"/>
            <param name="root.filename" expression="index"/>
            <param name="use.id.as.filename" expression="1"/>
            <xmlcatalog refid="docbook.catalog"/>
        </style>
    </target>

    <target name="docbook-html-doc" depends="init, docbook-init">
        <foreach target="docbook-html-doc-single" list="${project.docbook.filelist}" param="docbook.file"/>
    </target>

    <target name="docbook-pdf-doc-single" depends="init, docbook-init">
        <mkdir dir="${output.docbook.fop}"/>
        <copy file="${project.docbook}/style/fop-xsl.template" tofile="${docbook.fop.xsl}">
            <filterset>
                <filter token="docbook.xsl.base" value="${docbook.xsl.base}"/>
                <filter token="docbook.xsl.dir" value="${docbook.xsl.dir}"/>
                <filter token="format" value="docbook"/>
            </filterset>
        </copy>
        <replace token="\" value="/" file="${docbook.fop.xsl}"/>

        <copy todir="${output.docbook.fop}/standard-images">
            <fileset dir="${project.docbook}/images">
                <include name="callouts/**/*.gif"/>
            </fileset>
        </copy>
        <copy todir="${output.docbook.fop}/common-images">
            <fileset dir="${project.docbook}/images">
                <include name="navigation/**/*.gif"/>
                <include name="admon/**/*.gif"/>
            </fileset>
            <mapper type="flatten"/>
        </copy>

        <mkdir dir="${output.docbook.pdf}"/>

        <!-- copy project-local images (if they exist) -->
        <copy todir="${output.docbook.fop}">
            <fileset dir="${project.docbook}">
                <include name="images/**/*.gif"/>
                <include name="images/**/*.jpg"/>
                <include name="images/**/*.jpeg"/>
            </fileset>
        </copy>

        <uptodate property="uptodate.fop" targetfile="${output.docbook.fop}/${docbook.file}.fop">
            <srcfiles dir="${project.docbook}">
                <include name="*.xml"/>
            </srcfiles>
        </uptodate>
        <antcall target="docbook-generate-fop"/>

        <uptodate property="uptodate.pdf" targetfile="${output.docbook.pdf}/${docbook.file}.pdf">
            <srcfiles dir="${project.docbook}">
                <include name="*.xml"/>
            </srcfiles>
        </uptodate>
        <antcall target="docbook-convert-fo-to-pdf"/>
    </target>

    <target name="docbook-generate-fop" unless="uptodate.fop">
        <style  destdir="${output.docbook.fop}"
                style="${docbook.fop.xsl}"
                processor="trax"
                force="true"
                in="${project.docbook}/${docbook.file}.xml"
                out="${output.docbook.fop}/${docbook.file}.fop">
            <param name="base.dir" expression="${output.docbook.html}/${docbook.file}/"/>
            <param name="project.docbook.file" expression="${docbook.file}"/>
            <param name="use.id.as.filename" expression="1"/>
            <xmlcatalog refid="docbook.catalog"/>
        </style>
    </target>

    <target name="docbook-convert-fo-to-pdf" unless="uptodate.pdf">
        <echo>Converting FO to PDF ...</echo>
        <java classname="org.apache.fop.apps.Fop" fork="true">
            <classpath>
                <fileset dir="${fop.dir}">
                    <include name="**/*.jar"/>
                </fileset>
            </classpath>
            <arg line="-q"/>
            <arg line="-fo ${output.docbook.fop}/${docbook.file}.fop"/>
            <arg line="-pdf ${output.docbook.pdf}/${docbook.file}.pdf"/>
        </java>
    </target>

    <target name="docbook-pdf-doc" depends="init, docbook-init">
        <foreach target="docbook-pdf-doc-single" list="${project.docbook.filelist}" param="docbook.file"/>
    </target>

    <property name="output.doc.jar"             location="${output.jars}/${project.name}${project.version.suffix}-doc.zip"/>

    <!-- path to source in "java" and source generated by javacc -->
    <path id="gen.java.path">
        <path refid="javadoc.default.java.path"/>
        <pathelement location="${output.javacc.gen}"/>
    </path>

    <!-- **************************************************************** -->
    <!-- ************************ JUnit Testing ************************* -->
    <!-- **************************************************************** -->

    <property name="output.test"        value="${output.root}/test"/>

    <!-- **************************************************************** -->
    <!-- ************************ Build Targets ************************* -->
    <!-- **************************************************************** -->

	<target name="clean" description="Clean output of build">
        <echo message="Cleaning ${project.name}"/>
        <delete dir="${output.root}"/>
	</target>

	<target name="build" description="Build OGNL">
	    <antcall target="javacc-build">
	        <param name="javacc.jjt" value="${project.java}/ognl/ognl.jjt"/>
	        <param name="javacc.gen" value="${project.javacc}"/>
	        <param name="javacc.pkg" value="ognl"/>
	    </antcall>

        <antcall target="javac-compile">
            <reference torefid="javac.java.path" refid="gen.java.path"/>
        </antcall>
	</target>

	<target name="jar" depends="build" description="Build OGNL jar distribution">
	    <patternset id="test.excludes.path">
	        <exclude name="ognl/test/**"/>
	    </patternset>
	    <antcall target="jar-build">
	        <reference torefid="jar.files.pattern" refid="test.excludes.path"/>
	    </antcall>
	</target>

	<target name="install" description="Install OGNL jar">
	    <antcall target="jar">
	        <param name="output.jars" value="${output.install.jars}"/>
	        <param name="output.jar" value="${output.install.jars}/${project.jar}"/>
	    </antcall>
	</target>

    <target name="doc-docbook-html" depends="docbook-html-doc" description="Build OGNL DocBook HTML documentation"/>

    <target name="doc-docbook-pdf" depends="docbook-pdf-doc" description="Build OGNL DocBook PDF documentation"/>

    <target name="doc-docbook" depends="doc-docbook-html, doc-docbook-pdf" description="Build all OGNL DocBook documentation"/>

    <target name="doc-javadoc" depends="build" description="Build OGNL API documentation">
	    <antcall target="javadoc-doc">
	        <reference torefid="javadoc.java.path" refid="gen.java.path"/>
	    </antcall>
    </target>

	<target name="doc" depends="doc-javadoc, doc-docbook" description="Build OGNL API and DocBook (HTML and PDF) documentation"/>

    <target name="doc-dist" depends="doc" description="Builds OGNL documentation distribution jar">
        <dirname property="pdfdir" file="${output.docbook.pdf}"/>
        <basename property="pdffile" file="${output.docbook.pdf}"/>
        <dirname property="htmldir" file="${output.docbook.html}"/>
        <basename property="htmlfile" file="${output.docbook.html}"/>
        <dirname property="apidir" file="${output.javadoc}"/>
        <basename property="apifile" file="${output.javadoc}"/>
        <zip zipfile="${output.doc.jar}">
            <fileset dir="${pdfdir}">
                <include name="${pdffile}/**/*"/>
            </fileset>
            <fileset dir="${htmldir}">
                <include name="${htmlfile}/**/*"/>
            </fileset>
            <fileset dir="${apidir}">
                <include name="${apifile}/**/*"/>
            </fileset>
        </zip>
    </target>

	<target name="dist" depends="jar-dist" description="Build OGNL distribution jar"/>

	<target name="compile-tests" depends="build" description="Compile OGNL unit tests">
	    <path id="test.java.path">
	        <pathelement location="${project.root}/test/java"/>
	    </path>
	    <antcall target="javac-compile">
            <reference torefid="javac.java.path" refid="test.java.path"/>
		</antcall>
    </target>

	<target name="test" depends="build, compile-tests" description="Run OGNL unit tests">
	    <mkdir dir="${output.test}/xml"/>
        <junit printsummary="true" haltonfailure="false">
            <classpath refid="javac.default.class.path"/>
            <formatter type="brief" usefile="false"/>
            <formatter type="xml"/>
            <batchtest todir="${output.test}/xml">
                <fileset dir="${output.classes}" includes="org/ognl/test/*Test.class"/>
            </batchtest>
        </junit>

	    <mkdir dir="${output.test}/html"/>
        <junitreport todir="${output.test}/html">
            <fileset dir="${output.test}/xml">
                <include name="TEST-*.xml"/>
            </fileset>
            <report format="frames" todir="${output.test}/html"/>
        </junitreport>
    </target>

    <target name="performance" depends="compile-tests">
		<java classname="org.ognl.test.Performance" fork="true">
			<classpath>
			    <path refid="javac.default.class.path"/>
			    <pathelement location="${ant.home}/lib/junit.jar"/>
			</classpath>
			<arg value="${param1}"/>
			<arg value="${param2}"/>
			<arg value="${param3}"/>
			<arg value="${param4}"/>
		</java>
    </target>

</project>
