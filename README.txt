* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*                                                                           *
*   Copyright (c) 1998-2004, Drew Davidson and Luke Blanshard               *
*                     All rights reserved.                                  *
*                                                                           *
*   Redistribution and use in source and binary forms, with or without      *
*   modification, are permitted provided that the following conditions are  *
*   met:                                                                    *
*                                                                           *
*   Redistributions of source code must retain the above copyright notice,  *
*   this list of conditions and the following disclaimer.                   *
*   Redistributions in binary form must reproduce the above copyright       *
*   notice, this list of conditions and the following disclaimer in the     *
*   documentation and/or other materials provided with the distribution.    *
*   Neither the name of the Drew Davidson nor the names of its contributors *
*   may be used to endorse or promote products derived from this software   *
*   without specific prior written permission.                              *
*                                                                           *
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
*   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
*   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       *
*   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE          *
*   COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,     *
*   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,    *
*   BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS   *
*   OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED      *
*   AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,  *
*   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF   *
*   THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH    *
*   DAMAGE.                                                                 *
*                                                                           *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

1. Overview
===========

OGNL is a binding and expression language for setting and getting properties
of Java objects.  It supports a Java-like syntax for navigating from
one JavaBean to another, plus it adds powerful collection operations
for manipulating Java Collections.

Some of the uses and features include:

  * OGNL was first conceived as a mechanism for binding parts of
    GUIs with model objects.  A single expression is used both to get
    the appropriate value from the model for display by a widget, and
    to set the newly edited value back into the model when editing is
    complete.  Expression set/get symmetry is a high priority with
    OGNL's syntax

  * The open source web frameworks Tapestry
    (http://jakarta.apache.org/tapestry) and WebWork
    (http://www.opensymphony.com) both use OGNL as their binding and
    expression language.

  * OGNL's current syntax includes most of Java's operators and a few
    of its own.  It is probably close to powerful enough to be used by
    a debugger or other system that requires run-time interpretation
    of expressions.

  * OGNL has fully integrated support for arbitrary-precision math, as
    embodied by the classes of the java.math package, meaning that
    OGNL's arithmetic operators work on BigIntegers and BigDecimals as
    well as the primitive types.


2. Building
===========

OGNL uses Ant 1.5.1 or above for it's build process.  The following Ant
targets are available:

    build             Build OGNL
    clean             Clean output of build
    test              Run OGNL unit tests
    jar               Build OGNL jar distribution
    dist              Build OGNL distribution zip archive
    doc-dist          Builds OGNL documentation distribution zip archive

    doc               Build OGNL API and DocBook (HTML and PDF) documentation
    doc-docbook       Build all OGNL DocBook documentation
    doc-docbook-html  Build OGNL DocBook HTML documentation
    doc-docbook-pdf   Build OGNL DocBook PDF documentation
    doc-javadoc       Build OGNL API documentation


The following packages are required by OGNL at build time.  They are
not required to run OGNL or be distributed along with OGNL.

The Version is the version that OGNL has been tested with.  If you
deviate from this you do so with the knowledge that you may have to
alter the build scripts or otherwise figure out a problem.  On the other
hand it may work out just fine.

The usage of the following jars are govered by the build.properties
file.  This is where locations of external tools can be configured.

All of the jars listed here normally will live in the "lib" directory
and the default build.properties references them there already, so this
would be the quickest way to get it running.  The docbook distributions
could possible live under lib, but the default build.properties
references them externally and the locations must be changed in that
file.

Note  Package              Version   Download Location
----  -------              -------   -----------------
  1   ant-contrib-0.3.jar      0.3   http://ant-contrib.sourceforge.net
  2   javacc.jar               2.1+  http://javacc.dev.java.net
  3   docbook-xml              4.2   http://www.oasis-open.org/docbook/xml
  3   docbook-xsl           1.65.0   http://docbook.sourceforge.net
  4   junit.jar              3.8.1   http://www.junit.org
  5   javassist.jar          2.6.0   http://www.jboss.org/developers/projects/javassist.html


(1) These jars are required to build OGNL and the JavaDoc documentation.
(2) JavaCC comes in several publicly released versions, each with a
    different package name for the tools.  By default the build.properties
    file specifies the 3.2 version.  The build file will then read
    properties from the javacc-<version>.properties.  If a different
    version is used then the build.properties file needs to be
    altered to reflect that.  IT IS RECOMMENDED THAT YOU GET THE LATEST
    VERSION OF JAVACC.
(3) The docbook distributions are only required if you wish to build
    the DocBook-based documentation.  You must get the entire XML or
    XSL distribution.
(4) The JUnit jar is needed to run the tests and must be installed in
    the Ant installation's lib directory.
(5) Javassist is only required for running the tests because of the
    org.ognl.test.CompilingPropertyAccessor.


3. Documentation
================

OGNL is documented in the accompanying file "javadoc/package.html", which is
used by javadoc as the ognl package description.  The best way to view
this is to generate javadoc (using "ant doc" of just "ant doc-javadoc"),
and then open the generated file "build/javadoc/index.html".
The package description will be below the lists of interfaces and classes.

OGNL API and Guide documentation is available at the OGNL website

    http://www.ognl.org


4. Feedback
===========

If you have any observations or complaints about OGNL or its
implementation or documentation, we would like to hear from you.
Drew Davidson at drew@ognl.org and Luke Blanshard by email at
blanshlu@netscape.net.


5. Mailing Lists
================

OGNL Technology, Inc. (http://www.ognl.org) runs several mailing
lists that may be of interest to people using and developing
with OGNL:

ognl-announce@lists.ognl.org

    A moderated list used to announce new versions of OGNL,
    documentation, example code, etc. to the OGNL community.

          Subscribe: ognl-announce-subscribe@lists.ognl.org
        Unsubscribe: ognl-announce-unsubscribe@lists.ognl.org
               Help: ognl-announce-help@lists.ognl.org


ognl-interest@lists.ognl.org

    An open list for discussion topics of general interest to the
    OGNL community. Use this list to ask questions, discuss tips
    and best practices, as well as requests for new features.

          Subscribe: ognl-interest-subscribe@lists.ognl.org
        Unsubscribe: ognl-interest-unsubscribe@lists.ognl.org
               Help: ognl-interest-help@lists.ognl.org


ognl-developer@lists.ognl.org

    An open list for discussions about developer topics in OGNL
    Bugs in the framework that are discovered should also be reported
    here.

          Subscribe: ognl-developer-subscribe@lists.ognl.org
        Unsubscribe: ognl-developer-unsubscribe@lists.ognl.org
               Help: ognl-developer-help@lists.ognl.org

